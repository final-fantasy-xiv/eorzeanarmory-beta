import fs from 'node:fs';
import path from 'node:path';
import url from 'node:url';
import { parse } from 'csv-parse/sync';
import slugify from 'slugify';

const __moduleURL = new URL(import.meta.url);
const __moduleDir = path.dirname(url.fileURLToPath(__moduleURL));

const __csvPath = path.join(__moduleDir, "..", "..", "Item Lists");

// columns we care about
const itemsColumns = {
    "id": 0,
    "name": 1,
    "allClassJobs": 2,
    "classJobString": 3,
    "screenshotClassJob": 4,
    "equipLevel": 5,
    "itemLevel": 6,
    "equipSlotCategory": 7,
    "mainModel": 8,
    "subModel": 9,
    "modelString": 10,
    "isFirstOfModel": 11,
    "sharedModels": 12,
    "colorString": 13,
    "isFirstOfColor": 14,
    "alternateVersions": 15,
    "webName": 16,
    "lodestoneID": 17,
    "isCrestWorthy": 18,
    "isDyeable": 19,
    "isGlamourous": 20,
    "isUnique": 21,
    "isUntradable": 22,
    "gallery": 23,
    "isInMultipleGalleries": 24
}

function loadItems(data) {
    let items = [];
    let modelsProcessed = new Set();
    // in theory `data` might have what we need, but I'm not sure how eleventy
    // decides what order to load things in, so we'll just read the gallery
    // data ourselves
    const galleryJson = fs.readFileSync(path.join(__moduleDir, "gallery_data.json"));
    const galleryData = JSON.parse(galleryJson);
    const itemsTsv = fs.readFileSync(path.join(__csvPath, 'FormattedItems.tsv'));   // possibly slow but I don't know how to do async in 11ty
    const itemRecords = parse(itemsTsv, {bom: true, delimiter: "\t"});
    // For each item in the item list, loop through our defined galleries until we find one that matches the item's.
    for (const record of itemRecords) {
        var itemGalleries = record[itemsColumns.gallery].split(", ")
        if (record[itemsColumns.isFirstOfColor] == "True") {
            for (const gallery of galleryData) {
                if (itemGalleries.includes(gallery.name)) {
                    // Once we've found the gallery that the item fits into, assign all the item's properties
                    let item = {};
                    item.gallery = gallery.name;
                    item.path = gallery.path;
                    item.id = record[itemsColumns.id];
                    item.name = record[itemsColumns.name];
                    // Certain item types have multiple images, rather than just a single one.
                    if (gallery.hasOwnProperty("images")) {
                        item.images = gallery.images;
                    }
                    item.equipLevel = record[itemsColumns.equipLevel];
                    item.modelMain = record[itemsColumns.modelMain];
                    item.modelMainKey = item.path + "###" + item.modelMain;
                    item.slug = record[itemsColumns.webName]
                    item.isFirstOfModel = record[itemsColumns.isFirstOfModel]
                    const showJobs = ["PLD", "WHM", "BLM"]
                    if (record[itemsColumns.equipSlotCategory] == "2" && !showJobs.some(j => record[itemsColumns.classJobString].includes(j)) )
                    {
                        // Temporary fix to hide off-hand gear until we have images of them
                        item.ShowInGallery = false
                    }
                    else
                    {
                        item.ShowInGallery = true
                    }

                    // Add our new item to the list
                    items.push(item);

                    break;
                }
            }
        }
    }
    return items;
}

export default loadItems;