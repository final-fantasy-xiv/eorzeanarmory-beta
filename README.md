# Eorzean Armory

This is the repository for the Eorzean Armory site. It's still under construction at the moment, but feel free to suggest any contributions you may have.

---

## Tech

We use [Eleventy](https://www.11ty.dev/) as our SSG. Additionally, we have some Python and AutoHotkey scripts around to help generate and format all the site's many assets.

## Running a test site

- `npm install`
- `npm run build`
- `npm run serve` to run the test server

## Updating site contents

- Run `PullAndFormatItemList.py` from the `Scripts` directory
  - This will grab the latest versions of the `Item.csv`, `ClassJobCategory.csv`, and `lodestone-item-ids.txt` files from their repositories. It will then merge information from all of them into a format more easily usable by the other site scripts and save them in `FormattedItems.tsv`. It should give you a list of any new items we'll need.
- Move `FormattedItems.tsv` out of `Output` and into `Scripts` (ie. up one directory). Also copy it to `Item Lists` in the main repo directory.
- Run `generateSkiplists.py`
  - This will generate separate lists for each item type and job, to be used with `Screenshot Loop.ahk`.
- If we need new items (or if changes have been made to the page template), run `GenerateItemPages.py`. This recreates every item's page.
  - Move the new pages over to `Content/` from `Scripts/Output/HTML`. You can just copy the three folders directly over and overwrite whatever's there.
- Set up poses in-game, using the information in the `Misc Info/` directory
- Run `Screenshot Loop.ahk` for each item type.
- Organize the photos into their own temporary file structure.
- Convert files to webp, then move __only the webp files for one job__ to `Scripts/Rename/` (You can delete the pngs at this point, or save them until you're sure you did it right)
- Modify `RenameFiles.py` to read the file for the correct job.
- Run `RenameFiles.py`
- Add the files to `content/images/` under the correct directory.
- Push the updated repo