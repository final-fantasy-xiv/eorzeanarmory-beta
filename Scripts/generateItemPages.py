# Open Template
# For each item with unique colors, create a page

import ast
import csv
import os.path
import string
from pathlib import Path

workingDirectory = Path.cwd() / 'Work Files'
outputDirectory = Path.cwd() / 'Output' / 'HTML'
# destinationFile = (workingDirectory / 'FormattedItems.tsv').resolve()
# if not destinationFile.exists():
#     destinationFile = open(destinationFile, 'w+')
currentDirectory = Path.cwd()
datafile = (currentDirectory/'FormattedItems.tsv').resolve()
if not datafile.exists():
	print("Could not find FormattedItems.tsv in current directory.")
	quit()

filteredColumns = ["#", "Name", "AllClassJobs", "ClassJobString", "ScreenshotClassJob", "Level{Equip}", "Level{Item}",
                   "EquipSlotCategory", "Model{Main}", "Model{Sub}", "ModelString", "IsFirstOfModel", "SharedModels",
                   "ColorString", "IsFirstOfColor", "AlternateVersions", "WebName", "LodestoneID", "IsCrestWorthy",
                   "IsDyeable", "IsGlamourous", "IsUnique", "IsUntradable", "Gallery", "IsInMultipleGalleries"]
allItems = []

# Create list for page variables
pageVariables = ['ItemName', 'FullPageURL', 'FullImageURL', 'AllClassJobs', 'EquipLevel', 'ItemLevel',
				 'RelativeImagePath', 'ItemTable', 'AlternateVersions', 'OutputPath', 'FileName', 'LodestoneLink']

pageDicts = []

def main():
	# Load in items
	print("Importing item data")
	importItems()

	# Create dict of strings to replace in page
	print("Generating page variables (this takes a few minutes)")
	for item in allItems:
		if item['IsFirstOfColor'] and item['GearType'] != 'Body':
			# Handle weapons equippable by multiple classes (eg. Scholar and Summoner)
			if item['GearType'] != 'Accessory':
				# Handle shields (so we don't duplicate them into Black and White Mage
				if item['Gallery'] == 'Shields':
					pageDicts.append((makePageDict(item, 'shields')))
				else:
					for classJob in item['AllClassJobs']:
						pageDicts.append(makePageDict(item, classJob))
			else:
				pageDicts.append(makePageDict(item, item['ScreenshotClassJob']))

	print("Generating pages")
	for page in pageDicts:
		createPage(page)
	return

def importItems():
	with open(datafile, 'r', encoding="utf_8") as openDataFile:
		reader = csv.reader(openDataFile, delimiter='\t')
		for row in reader:
			itemDict = processRow(row)
			allItems.append(itemDict)
	return

def processRow(rowToProcess):
	itemDict = {}
	for (key, value) in zip(filteredColumns, rowToProcess):
		if key.startswith('Is'):
			# Handle booleans
			itemDict[key] = value == 'True'
		elif key == 'AllClassJobs' or key == 'SharedModels' or key == 'AlternateVersions':
			# Handle lists
			itemDict[key] = ast.literal_eval(value) # TODO: Look into implementing this differently.
		else:
			# Everything else is a string
			itemDict[key] = value
	itemDict['GearType'] = getGearType(itemDict)
	return itemDict

def getGearType(itemDict):
	equipSlot = int(itemDict['EquipSlotCategory'])
	if equipSlot in [1, 2, 13]:
		if itemDict['ScreenshotClassJob'] in ['CRP', 'BSM', 'ARM', 'GSM', 'LTW', 'WVR', 'ALC', 'CUL', 'MIN', 'BTN', 'FSH']:
			return 'Tool'
		else:
			return 'Weapon'
	elif equipSlot in [9, 10, 11, 12]:
		return 'Accessory'
	else:
		return 'Body'

def makePageDict(item, classJob = ''):
	pageDict = {}
	for variable in pageVariables:
		match variable:
			case 'ItemName':
				pageDict[variable] = item['Name']
			case 'FullPageURL':
				pageDict[variable] = toFullPageURL(item['WebName'], item['GearType'], item['EquipSlotCategory'], classJob)
			case 'FullImageURL':
				pageDict[variable] = toFullImageURL(item['WebName'], item['GearType'], item['EquipSlotCategory'], classJob)
			case 'AllClassJobs':
				# pageDict[variable] = makeReadable(item['AllClassJobs'])
				pageDict[variable] = item['ClassJobString']
			case 'EquipLevel':
				pageDict[variable] = item['Level{Equip}']
			case 'ItemLevel':
				pageDict[variable] = item['Level{Item}']
			case 'RelativeImagePath':
				pageDict[variable] = toRelativeImageURL(item['WebName'], item['GearType'], item['EquipSlotCategory'], classJob)
			case 'ItemTable':
				pageDict[variable] = makeItemTable(item['SharedModels'])
			case 'AlternateVersions':
				pageDict[variable] = getAlternateColors(item['AlternateVersions'])
			case 'OutputPath':
				pageDict[variable] = getDirectory(item['GearType'], item['EquipSlotCategory'], classJob)
			case 'FileName':
				pageDict[variable] = item['WebName'] + '.html'
			case 'LodestoneLink':
				pageDict[variable] = getLodestoneLink(item['LodestoneID'])
			case _:
				# This should never come up
				print(f"You forgot to account for {variable}. Add a case to makePageDict()")
	return pageDict

def toFullPageURL(fileName, gearType, equipSlot, screenshotClassJob):
	fileName = fileName + '.html'
	returnString = f'https://eorzean-armory.com/{getDirectory(gearType, equipSlot, screenshotClassJob)}{fileName}'
	return returnString

def toFullImageURL(fileName, gearType, equipSlot, screenshotClassJob):
	if equipSlot == '9':
		# Earrings have multiple images in multiple folders. Return just the front one for now
		fileName = fileName.replace('.webp', '')
		returnString = f'https://eorzean-armory.com/images/{getDirectory(gearType, equipSlot, screenshotClassJob)}front/{fileName}-front.webp'
	elif equipSlot in ['10', '11', '12']:
		# All other accessories only have a single image
		returnString = f'https://eorzean-armory.com/images/{getDirectory(gearType, equipSlot, screenshotClassJob)}{fileName}.webp'
	else:
		# Weapons and Tools also have multiple images, but their showcase one is 'main'
		returnString = f'https://eorzean-armory.com/images/{getDirectory(gearType, equipSlot, screenshotClassJob)}{fileName}-main.webp'
	return returnString

def toRelativeImageURL(fileName, gearType, equipSlot, screenshotClassJob):
	if equipSlot == '9':
		# Earrings have multiple images, show the 'front' image as the main one
		returnString = f'/images/{getDirectory(gearType, equipSlot, screenshotClassJob)}{fileName}-front.webp'
	elif equipSlot in ['10', '11', '12']:
		# All other accessories only have a single image
		returnString = f'/images/{getDirectory(gearType, equipSlot, screenshotClassJob)}{fileName}.webp'
	else:
		# Weapons and Tools also have multiple images, but their showcase one is 'main'
		returnString = f'/images/{getDirectory(gearType, equipSlot, screenshotClassJob)}{fileName}-main.webp'
	return returnString

def getDirectory(gearType, equipSlot, screenshotClassJob):
	returnString = ''
	match gearType:
		case 'Accessory':
			returnString = 'accessories/'
			match equipSlot:
				case '9':
					returnString += 'earrings/'
				case '10':
					returnString += 'necklaces/'
				case '11':
					returnString += 'bracelets/'
				case '12':
					returnString += 'rings/'
		case 'Body':
			returnString = 'body/' + screenshotClassJob.lower() + '/'
		case 'Tool':
			returnString =  'tools/' + screenshotClassJob.lower() + '/'
		case 'Weapon':
			if equipSlot == '2' and screenshotClassJob == "PLD":
				returnString = 'weapons/shields/'
			else:
				returnString = 'weapons/' + screenshotClassJob.lower() + '/'
	return returnString

def makeItemTable(sharedModels):
# Make Item Table
	itemTableString = '<table>\n<tr>\n<th class="text">Item</th>\n<th class="text">Jobs</th>\n<th class="number">Level</th>\n<th class="number">iLvl</th>\n</tr>\n'
	tableRows = 0
	for sharedItemNumber in sharedModels:
		tableRows += 1
		item = getItem(sharedItemNumber)
		itemTableString += '<tr>\n'
		# If item has lodestone ID, add lodestone link. Otherwise, leave it without one
		if item['LodestoneID'] == '':
			itemTableString += f'<td class="text">{item['Name']}</td>\n'
		else:
			itemTableString += f'<td class="text"><a href="{getLodestoneLink(item['LodestoneID'])}" class="eorzeadb_link">{item['Name']}</a></td>\n'
		itemTableString += f'<td class="text">{item['ClassJobString']}</td>\n'
		itemTableString += f'<td class="number">{item['Level{Equip}']}</td>\n'
		itemTableString += f'<td class="number">{item['Level{Item}']}</td>\n'
		itemTableString += '</tr>\n'
	if tableRows > 1:
		itemTableString += '</table>'
	else:
		itemTableString = '<p>This model is unique</p>'
	return itemTableString

def getLodestoneLink(lodestoneID):
	lodestoneLink = 'https://na.finalfantasyxiv.com/lodestone/playguide/db/item/' + str(lodestoneID) + '/'
	return lodestoneLink

def getItem(itemNumber):
	for item in allItems:
		if item['#'] == itemNumber:
			sharedItem = item
			break
	else:
		sharedItem = None
		print(f'Item #{itemNumber} not found?')
	return sharedItem

def makeReadable(allClassJobs):
	# AllClassJobs is a list of strings
	# TODO: Split combat jobs into categories too
	returnString = ''
	All = {'CRP', 'BSM', 'ARM', 'GSM', 'LTW', 'WVR', 'ALC', 'CUL', 'MIN', 'BTN', 'FSH', 'PLD', 'MNK', 'WAR', 'DRG', 'BRD', 'WHM', 'BLM', 'SMN', 'SCH', 'NIN', 'MCH', 'DRK', 'AST', 'SAM', 'RDM', 'BLU', 'GNB', 'DNC', 'RPR', 'SGE'}
	allDoHs = {'CRP', 'BSM', 'ARM', 'GSM', 'LTW', 'WVR', 'ALC', 'CUL'}
	allDoLs = {'MIN', 'BTN', 'FSH'}
	allDoHLs = {'CRP', 'BSM', 'ARM', 'GSM', 'LTW', 'WVR', 'ALC', 'CUL', 'MIN', 'BTN', 'FSH'}
	allCombatJobs = {'MNK', 'WAR', 'DRG', 'BRD', 'WHM', 'BLM', 'SMN', 'SCH', 'NIN', 'MCH', 'DRK', 'AST', 'SAM', 'RDM', 'BLU', 'GNB', 'DNC', 'RPR', 'SGE'}
	if All.issubset(allClassJobs):
		returnString = "All Classes and Jobs"
		allClassJobs.clear()
	elif allDoHLs.issubset(allClassJobs):
		returnString = "All Disciples of Hand and Land"
		for job in allDoHLs:
			allClassJobs.remove(job)
	elif allDoLs.issubset(allClassJobs):
		returnString = "All Disciples of Land"
		for job in allDoLs:
			allClassJobs.remove(job)
	elif allDoHs.issubset(allClassJobs):
		returnString = "All Disciples of Hand"
		for job in allDoHs:
			allClassJobs.remove(job)
	elif allCombatJobs.issubset(allClassJobs):
		returnString = "All Combat Jobs"
		for job in allCombatJobs:
			allClassJobs.remove(job)

	if len(allClassJobs) > 0:
		if returnString != '':
			returnString += " and "
		returnString = returnString + ", ".join(allClassJobs)

	return returnString

def getAlternateColors(alternateVersions):
	returnString = ''
	uniqueVariants = 0
	if len(alternateVersions) >= 1:
		for sharedItemNumber in alternateVersions:
			item = getItem(sharedItemNumber)
			if item['IsFirstOfColor']:
				uniqueVariants += 1
				itemPageLink = toFullPageURL(item['WebName'], item['GearType'], item['EquipSlotCategory'], item['ScreenshotClassJob'])
				itemImageLink = toRelativeImageURL(item['WebName'], item['GearType'], item['EquipSlotCategory'], item['ScreenshotClassJob'])
				returnString = returnString +  f'<a href="{itemPageLink.replace('.html', '/index.html')}"><img src="{itemImageLink}" class="thumbnail"></a>'
	if uniqueVariants < 1:
		returnString = 'There are no alternate versions of this model'
	return returnString

def createPage(pageDict):
	fileOutputDirectory = outputDirectory/pageDict['OutputPath']
	with open(workingDirectory/'template.html') as t:
		template = string.Template(t.read())
	outputText = template.safe_substitute(pageDict)
	if not os.path.exists(fileOutputDirectory):
		os.makedirs(fileOutputDirectory)
	with open(fileOutputDirectory/pageDict['FileName'], "w") as output:
		output.write(outputText)
	return

main()