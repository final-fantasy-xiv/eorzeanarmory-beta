# Get all files in directory
# Check filename against list
# Rename file to model name
import os
import csv
import os.path
from pathlib import Path

currentDirectory = Path.cwd()
workingPath = currentDirectory / 'Rename'
# Change below to whatever job you just shot (Remember accessories are singular)
nameFile = currentDirectory / 'Output' / 'Skiplists' / 'Filenames' / 'BLM.txt'
fileSuffix = '-main' # ie '-front' or '-main'

# Reads in filenames
with open(nameFile) as openDataFile:
	reader = csv.reader(openDataFile)
	filenames = list(reader)


dir_list = os.listdir(workingPath)
index = 0
for file in dir_list:
	if fileSuffix != '':
		filename = filenames[index][0].replace('.webp', '') + fileSuffix + '.webp'
		os.rename(workingPath / file, workingPath / filename)
	else:
		os.rename(workingPath / file, workingPath / filenames[index][0])
	index += 1