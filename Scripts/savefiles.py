# For (files in directory):
# 	For line in text:
# 		if filename == line
# 			keep file
# 		else
# 			delete file
import os

workingpath = 'D:\Dropbox\Tech\Websites\eorzean-armory - jewelry/earrings/right'
workinglist = 'D:\Dropbox\Tech\Websites\eorzean-armory - jewelry/Unique Earrings - Right.txt'

uniques = open(workinglist)
uniquelist = []
for item in uniques:
	uniquelist.append(item.rstrip('\n'))
dir_list = os.listdir(workingpath)
for item in dir_list:
	if(item in uniquelist):
		print('Found unique item ', item)
	else:
		print('Deleted duplicate item ', item)
		os.remove(workingpath + '/' + item)