# Pull the latest items.csv and classjobcategory.csv from the xivapi repo
# Also pull the latest Lodestone ID file from its repo
# Pull the relevant information from items.csv
# Add in Lodestone ID
# Save to FormattedItems.tsv

import csv
import os
from pathlib import Path
from time import time
import requests
from requests.exceptions import HTTPError
from collections import defaultdict
import re

# Set our global variables
skipDownload = False
# How long to wait before downloading a new copy of a file, in minutes. Change to 0 to always download
downloadDelay = 15 # Suggested: 15 minutes
workingDirectory = Path.cwd() / 'Work Files'
outputDirectory = Path.cwd() / 'Output'
destinationFile = (outputDirectory / 'FormattedItems.tsv').resolve()
if not destinationFile.exists():
    destinationFile = open(destinationFile, 'w+')
URLs = {'itemList': 'https://raw.githubusercontent.com/xivapi/ffxiv-datamining/master/csv/Item.csv',
        'classJobCategory': 'https://raw.githubusercontent.com/xivapi/ffxiv-datamining/master/csv/ClassJobCategory.csv',
        'lodestoneIDs': 'https://raw.githubusercontent.com/Asvel/ffxiv-lodestone-item-id/master/lodestone-item-id.txt'}
fileNames = {'itemList': 'Items.csv',
             'classJobCategory': 'ClassJobCategory.csv',
             'lodestoneIDs': 'lodestoneIDs.txt',
             'tempFile': 'tempfile.tmp',
             'formattedItems': 'FormattedItems.tsv'}
filePaths = {'itemList': workingDirectory / fileNames['itemList'],
             'classJobCategory': workingDirectory / fileNames['classJobCategory'],
             'lodestoneIDs': workingDirectory / fileNames['lodestoneIDs'],
             'tempFile': workingDirectory / fileNames['tempFile'],
             'formattedItems': outputDirectory / fileNames['formattedItems']}
changedPaths = {'itemList': workingDirectory / 'changes-itemList.txt',
                'classJobCategory': workingDirectory / 'changes-classJobCategory.txt',
                'lodestoneIDs': workingDirectory / 'changes-lodestoneIDs.txt',
                'tempFile': workingDirectory / 'changes-tempFile.txt',
                'formattedItems': workingDirectory / 'changes-formattedItems.txt'}
itemColumns = {"#", "Singular", "Adjective", "Plural", "PossessivePronoun", "StartsWithVowel", "Blank1", "Pronoun",
               "Article", "Description", "Name", "Icon", "Level{Item}", "Rarity", "FilterGroup", "AdditionalData",
               "ItemUICategory", "ItemSearchCategory", "EquipSlotCategory", "ItemSortCategory", "Blank2", "StackSize",
               "IsUnique", "IsUntradable", "IsIndisposable", "Lot", "Price{Mid}", "Price{Low}", "CanBeHq", "IsDyeable",
               "IsCrestWorthy", "ItemAction", "CastTime<s>", "Cooldown<s>", "ClassJob{Repair}", "Item{Repair}",
               "Item{Glamour}", "Desynth", "IsCollectable", "AlwaysCollectable", "AetherialReduce", "Level{Equip}",
               "RequiredPvpRank", "EquipRestriction", "ClassJobCategory", "GrandCompany", "ItemSeries",
               "BaseParamModifier", "Model{Main}", "Model{Sub}", "ClassJob{Use}", "Blank3", "Damage{Phys}",
               "Damage{Mag}", "Delay<ms>", "Blank4", "BlockRate", "Block", "Defense{Phys}", "Defense{Mag}",
               "BaseParam[0]", "BaseParamValue[0]", "BaseParam[1]", "BaseParamValue[1]", "BaseParam[2]",
               "BaseParamValue[2]", "BaseParam[3]", "BaseParamValue[3]", "BaseParam[4]", "BaseParamValue[4]",
               "BaseParam[5]", "BaseParamValue[5]", "ItemSpecialBonus", "ItemSpecialBonus{Param}",
               "BaseParam{Special}[0]", "BaseParamValue{Special}[0]", "BaseParam{Special}[1]",
               "BaseParamValue{Special}[1]", "BaseParam{Special}[2]", "BaseParamValue{Special}[2]",
               "BaseParam{Special}[3]", "BaseParamValue{Special}[3]", "BaseParam{Special}[4]",
               "BaseParamValue{Special}[4]", "BaseParam{Special}[5]", "BaseParamValue{Special}[5]", "MaterializeType",
               "MateriaSlotCount", "IsAdvancedMeldingPermitted", "IsPvP", "SubStatCategory", "IsGlamourous"}
columnsToKeep = ["#", "Name", "Level{Equip}", "Level{Item}", "EquipSlotCategory", "IsUnique", "IsUntradable",
                 "IsDyeable", "IsCrestWorthy", "ClassJobCategory", "Model{Main}", "Model{Sub}", "IsGlamourous"]
filteredColumns = ["#", "Name", "AllClassJobs", "ClassJobString", "ScreenshotClassJob", "Level{Equip}", "Level{Item}",
                   "EquipSlotCategory", "Model{Main}", "Model{Sub}", "ModelString", "IsFirstOfModel", "SharedModels",
                   "ColorString", "IsFirstOfColor", "AlternateVersions", "WebName", "LodestoneID", "IsCrestWorthy",
                   "IsDyeable", "IsGlamourous", "IsUnique", "IsUntradable", "Gallery", 'IsInMultipleGalleries']
classesToSkip = ["ADV", "GLA", "PGL", "MRD", "LNC", "ARC", "CNJ", "THM", "ACN", "ROG"]
classJobAbbreviations = {'war': 'Warrior', 'pld': 'Paladin', 'drk': 'Dark Knight', 'gnb': 'Gunbreaker',
                 'whm': 'White Mage', 'ast': 'Astrologian', 'sch': 'Scholar', 'sge': 'Sage',
                 'nin': 'Ninja', 'sam': 'Samurai', 'mnk': 'Monk', 'rpr': 'Reaper', 'drg': 'Dragoon',
                 'mch': 'Machinist', 'brd': 'Bard', 'dnc': 'Dancer',
                 'smn': 'Summoner', 'rdm': 'Red Mage', 'blm': 'Black Mage', 'blu': 'Blue Mage',
                 'crp': 'Carpenter', 'cul': 'Culinarian', 'arm': 'Armorer', 'bsm': 'Blacksmith',
                 'alc': 'Alchemist', 'ltw': 'Leatherworker', 'gsm': 'Goldsmith', 'wvr': 'Weaver',
                 'btn': 'Botanist', 'min': 'Miner', 'fsh': 'Fisher'}
preferredJobOrder = []
lodestoneIDList = [""] # LodestoneIDs is *not* zero indexed, so initializing with a value to cheat when using .append()
classJobCategoryList = []
classJobStrings = []
filteredItems = []
sharedModels = defaultdict(list)
sharedColors = defaultdict(list)

def pullFiles():
    # Pull the files we'll need
    for url in URLs:
        # Check if file exists and is recent. If so, don't download a new version
        if filePaths[url].exists():
            lastMod = os.path.getmtime(filePaths[url])
            timeSinceLastMod = int((time() - lastMod)/60)
            if timeSinceLastMod < downloadDelay:
                print(fileNames[url] + " exists, and was last modified " + formatTime(timeSinceLastMod) + " ago. Skipping download.")
            else:
                print(fileNames[url] + " exists, but was last modified " + formatTime(timeSinceLastMod) + " ago. Downloading new copy.")

                if skipDownload:
                    print("Manually skipping download of " + fileNames[url])
                else:
                    tryDownload(url)
        else:
            tryDownload(url)
        print()  # Split each file's output

def formatTime(minutes = 0):
    # Format the time into minutes, hours, or days
    returnString = ''
    if minutes < 60:
        returnString = str(minutes) + " minute"
        if minutes != 1:
            returnString = returnString + "s"
    elif minutes < 1440:
        returnString = str(int(minutes/60)) + " hour"
        if int(minutes/60) != 1:
            returnString = returnString + "s"
    else:
        returnString = str(int(minutes/1440)) + " day"
        if int(minutes/1440) != 1:
            returnString = returnString + "s"
    return returnString

def tryDownload(url):
    try:
        response = requests.get(URLs[url])
        response.raise_for_status()
    except HTTPError as http_err:
        print(f"HTTP error occurred: {http_err}")
    except Exception as err:
        print(f"Other error occurred: {err}")
    else:
        print("Successfully downloaded " + fileNames[url])
        saveToFile(response.text, url)

def saveToFile(contents, file):
    if not filePaths[file].exists():
        writeFile(contents, filePaths[file], file)
    else:
        # If we already have a list of items (we should), compare the new list against the old one.
        # Should always replace the old file (so we have timestamps for the last time we checked)
        # and print any differences
        print(fileNames[file] + " exists. Saving data to temporary file and checking for updates.")
        writeFile(contents, filePaths['tempFile'], 'tempFile')
        diffCheck(filePaths[file], filePaths['tempFile'], file)
        os.remove(filePaths[file])
        os.rename(filePaths['tempFile'], filePaths[file])
    return

def writeFile(contents, filePath, fileName, split = True):
    if filePath.exists():
        # TODO: Consider rewriting this to save old files (maybe append dates to filenames?)
        os.remove(filePath)
    outfile = open(filePath, 'x')

    # Some contents can be in lists of strings, rather than strings that need split.
    if split:
        splitContents = contents.splitlines()
    else:
        splitContents = contents

    for line in splitContents:
        outfile.write(str(line) + '\n')
    outfile.close()
    print("Succesfully saved " + fileName)

def diffCheck(oldFilePath, newFilePath, file):
    oldFileList = listify(oldFilePath)
    newFileList = listify(newFilePath)

    index = 0
    changedItems = []
    addedItems = []

    while index < len(newFileList):
        # Check for differences line by line
        if index >= len(oldFileList):
            addedItems.append(newFileList[index])
        elif oldFileList[index] != newFileList[index]:
            changedItems.append(oldFileList[index])
            changedItems.append(newFileList[index])
        index += 1
    # Write any changes to a changelog file and print alert
    changeCount = int((len(changedItems)/2) + len(addedItems))
    if changeCount > 0:
        returnString = f"{changeCount} changes detected\n---\n"
        if len(changedItems) > 0:
            returnString = returnString + f"{int(len(changedItems)/2)} item(s) were changed.\n"
        if len(addedItems) > 0:
            returnString = returnString + f"{len(addedItems)} item(s) were added.\n"
            changedItems.append("---\nAdded Items\n---\n")
            for line in addedItems:
                changedItems.append(line)
        writeFile(changedItems, changedPaths[file], file, False)
        returnString = returnString + f"Changelog can be found at {changedPaths[file]}"
        print(returnString)
    else:
        print("No changes found.")

def listify(filepath):
    # Opens a file and returns its contents as a list
    fileContents = open(filepath, 'r')
    fileList = []
    for line in fileContents:
        fileList.append(line)
    return fileList

def formatInfo():
    # Save the relevant columns we want from Items.csv, add in lodestone ID, and translate ClassJobCategory
    # Parse contents of a response into a file we can use
    columnNames = []
    contents = open(filePaths['itemList'], newline='')
    separatedContents = csv.reader(contents, delimiter=',')
    print("Formatting Items")
    for row in separatedContents:
        # Column names are in the second row, use those instead of pre-defining the list
        # (In case new columns are added, or names change)
        if separatedContents.line_num == 2:
            columnNames = row

        # Skip the first three lines of the item.csv, they're just table header stuff
        if separatedContents.line_num > 3:
            # Read the item in as a dict, so we can refer to columns by name
            itemDict = {k: v for (k, v) in zip(columnNames, row)}
            filteredDict = defaultdict(list)
            filteredItem = []
            for column in columnsToKeep:
                # Certain columns need extra handling
                match column:
                    case 'ClassJobCategory':
                        if itemDict['Model{Main}'] == '9019, 1, 1, 0':
                            filteredDict['AllClassJobs'].append([])
                        else:
                            filteredDict['AllClassJobs'].append(classJobCategoryList[int(itemDict['ClassJobCategory'])])
                        filteredDict['ClassJobString'].append(getClassJobString(int(itemDict['ClassJobCategory'])))
                        filteredDict['ScreenshotClassJob'].append(translateClassJobCategory(itemDict['ClassJobCategory']))
                    case 'Model{Main}':
                        filteredDict['Model{Main}'].append(itemDict['Model{Main}'])
                        firsts = addUpdateSharedModels(itemDict['Model{Main}'], itemDict['#'], itemDict['EquipSlotCategory'], itemDict['Name'])
                        filteredDict['IsFirstOfModel'] = firsts[0]
                        filteredDict['ModelString'] = firsts[1]
                        filteredDict['IsFirstOfColor'] = firsts[2]
                        filteredDict['ColorString'] = firsts[3]
                    case _:
                        filteredDict[column].append(itemDict[column])
            # Make WebName
            # Save filtered item to the list
            filteredDict['LodestoneID'].append(findLodestoneID(itemDict['#'], itemDict['Name']))
            filteredDict['WebName'].append(generateFileName(itemDict['#'], itemDict['Name']))
            galleryValues = getItemGallery(itemDict['EquipSlotCategory'], filteredDict['AllClassJobs'][0])
            filteredDict['Gallery'] .append(galleryValues[0])
            filteredDict['IsInMultipleGalleries'] = galleryValues[1]
            for column in filteredColumns:
                if (column == 'SharedModels' or column == 'AlternateVersions' or column == 'IsFirstOfModel' or
                        column == 'IsFirstOfColor' or column == 'ModelString' or column == 'ColorString' or
                        column == 'IsInMultipleGalleries'):
                    filteredItem.append(filteredDict[column])
                elif column.startswith('Is'):
                    appendRow = filteredDict[column]
                    filteredItem.append(appendRow[0] == 'True')
                else:
                    appendRow = filteredDict[column]
                    filteredItem.append(appendRow[0])
            filteredItems.append(filteredItem)
    # Add Shared Model and Color lists to items
    print('Updating Shared Model and Color lists')
    for item in filteredItems:
        # Populate SharedModels
        item[12] = getSharedModels(item[10])
        # Populate AlternateVersions
        item[15] = getAlternateVersions(item[10], item[13])

    # Save list to file
    print('Saving ' + fileNames['formattedItems'])
    with open(filePaths['formattedItems'], "w") as f:
        wr = csv.writer(f, delimiter='\t')
        wr.writerows(filteredItems)

    contents.close()
    return

def translateClassJobCategory(classJobCategory):
    # ClassJobCategory has any number of classes and jobs in a given line. We only need one.
    index = int(classJobCategory)
    if len(classJobCategoryList[index]) > 0:
        jobToUse = classJobCategoryList[index][0]
    else:
        jobToUse = ''
    return jobToUse

def getClassJobString(classJobCategory):
    # TODO: Process these strings some, to remove classes
    return classJobStrings[classJobCategory]

def findLodestoneID(itemNumber, itemName):
    index = int(itemNumber)
    # Catch index out of bounds error
    if index >= len(lodestoneIDList):
        return ''
    else:
        return lodestoneIDList[index]
    #if lodestoneID == '' and itemName != '':
        # If item has name but no lodestone ID, print it
        # TODO: Rewrite this so we don't completely take over the console
        #print("No Lodestone ID found for item #" + itemNumber + " - " + itemName)

def generateFileName(itemNumber, itemName):
    # Format: [Item #]-[Item Name].webp, replace all spaces with dashes
    # Also replace non-alphanumeric characters (with what?)
    strippedItemName = itemName.strip()
    strippedItemName = strippedItemName.replace(" ", "-")
    strippedItemName = re.sub('[^0-9a-zA-Z-]+', '', strippedItemName)
    returnString = itemNumber.zfill(5) + "-" + strippedItemName.lower()
    return returnString

def addUpdateSharedModels(modelString, itemNumber, equipSlot, itemName):
    # Return True if is first of model and/or colorway
    # Also return model and color strings

    # Don't save items with model [0,0,0,0] or model [9019,1,1,0]
    if modelString == '0, 0, 0, 0' or modelString == '9019, 1, 1, 0':
        return [False, '', False, '']
    # First, filter items by name (Dated, Aetherial, etc)
    # We want to completely skip them so they don't show up when we create our lists
    ignoreStrings = ['Dated', 'Aetherial']
    for string in ignoreStrings:
        if itemName.startswith(string):
            return [False, '', False, '']

    # The last value used is for colorway, the other(s) are for the actual model.
    # Because of this, we have to build specific strings to use as keys for the shared dicts
    splitModel = modelString.split(", ")
    # Weapons and Food seem to use 3 values in modelString (the 3rd is color)
    if equipSlot == '1' or equipSlot == '2' or equipSlot == '13' or splitModel[0] == '9901':
        modelString = equipSlot + " - " + splitModel[0] + ", " + splitModel[1] + ", 0, 0"
        colorString = equipSlot + " - " + splitModel[0] + ", " + splitModel[1] + ", " + splitModel[2] + ", 0"
    # All other items seem to use 2 values (the 2nd is color)
    else:
        modelString = equipSlot + " - " + splitModel[0] + ", 0, 0, 0"
        colorString = equipSlot + " - " + splitModel[0] + ", " + splitModel[1] + ", 0, 0"

    # Add the item to wherever in the dicts it should go, then check what items are on that key.
    # If it's the only item for that key, return true (is first of its entry). Otherwise, return false
    sharedModels[modelString].append(itemNumber)
    sharedColors[colorString].append(itemNumber)
    modelItems = ''
    colorItems = ''

    for item in sharedModels[modelString]:
        modelItems = modelItems + item
    for item in sharedColors[colorString]:
        colorItems = colorItems + item
    return [modelItems==itemNumber, modelString, colorItems==itemNumber, colorString]

def getSharedModels(modelString):
    if modelString == '':
        return ['']
    else:
        return sharedModels[modelString]

def getSharedColors(colorString):
    if colorString == '':
        return ['']
    else:
        return sharedColors[colorString]

def getAlternateVersions(modelString, colorString):
    # Return all items where modelString matches, colorString does *not*, and isFirstOfColor == true
    # IsFirstOfModel == item[11], IsFirstOfColor == item[14]
    alternateVersions = []
    if modelString == '':
        return ['']
    for sharedModel in sharedModels[modelString]:
        item = filteredItems[int(sharedModel)]
        if item[14] == True and item[13] != colorString:
            alternateVersions.append(sharedModel)
    return alternateVersions

def getItemGallery(equipSlotCategory, allClassJobs):
    # Return the gallery (or galleries) an item should be present in, as well as if it is in multiple galleries
    galleryString = ''
    galleries = []
    equipSlot = int(equipSlotCategory)
    if equipSlot in [9, 10, 11, 12]:
        match equipSlot:
            case 9:
                galleries.append('Earrings')
                galleryString = 'Earrings'
            case 10:
                galleries.append('Necklaces')
                galleryString = 'Necklaces'
            case 11:
                galleries.append('Bracelets')
                galleryString = 'Bracelets'
            case 12:
                galleries.append('Rings')
                galleryString = 'Rings'
    elif equipSlot in [1, 2, 13]:
        # TODO: Add handling for shields
        usesShields = ["PLD", "WHM", "BLM"]
        if equipSlot == 2 and any(shieldUser in allClassJobs for shieldUser in usesShields):
            galleryString = 'Shields'
        else:
            for classJob in allClassJobs:
                galleries.append(classJobAbbreviations[classJob.lower()])
                if galleryString == '':
                    galleryString = classJobAbbreviations[classJob.lower()]
                else:
                    galleryString = galleryString + ', ' + classJobAbbreviations[classJob.lower()]


    return [galleryString, len(galleries) > 1]

def setup():
    # Populate list of Lodestone IDs and ClassJobCategories (So we don't do it each item)
    lodestoneIDContents = open(filePaths['lodestoneIDs'], 'r')
    separatedContents = csv.reader(lodestoneIDContents, delimiter=',')
    for line in separatedContents:
        if len(line) > 0:
            lodestoneIDList.append(line[0])
        else:
            lodestoneIDList.append('')
    lodestoneIDContents.close()

    classJobCategoryContents = open(filePaths['classJobCategory'], 'r')
    separatedContents = csv.reader(classJobCategoryContents, delimiter=',')
    classJobColumns = []
    for line in separatedContents:
        filteredClassJobs = []
        # Because of how the original csv is formatted, we need to do some interpretation
        if separatedContents.line_num == 2:
            # Populate list of Class and Job names
            for column in line:
                classJobColumns.append(column)
        # Skip the first few lines of table header stuff
        if separatedContents.line_num >= 4:
            index = 0
            for column in line:
                # Run through each class/job labeled as true and make sure we're not skipping them
                # If not, add them to the list of classes for that line
                if column == "True" and not classJobColumns[index] in classesToSkip:
                    filteredClassJobs.append(classJobColumns[index])
                index = index + 1
            classJobCategoryList.append(filteredClassJobs)
            classJobStrings.append(line[1])
    classJobCategoryContents.close()
    return

def main():
    pullFiles()
    setup()
    formatInfo()

main()