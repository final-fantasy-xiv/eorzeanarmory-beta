﻿#SingleInstance Force
; #Warn  ; Enable warnings to assist with detecting common errors.
SetWorkingDir A_ScriptDir  ; Ensures a consistent starting directory.
SetWorkingDir "Output\Skiplists\"
CoordMode "Mouse", "Screen"
CoordMode "Pixel", "Screen"
SetKeyDelay 250, 75

global isDryRun := false ; Set true to perform all actions *except* the actual taking of screenshots

; Instead of needing to change the filename every time, just open a file select dialog to grab the skiplist
skiplistPath := FileSelect(,, "Choose a skiplist file", "Text Document (*.txt)")

if (skiplistPath = "") {
    MsgBox "The user didn't select anything."
	ExitApp
}
else {
	skiplistFile := FileRead(skiplistPath, "`n")
	SplitPath skiplistPath,,,, &skiplistName
    global fileContents := StrSplit(skiplistFile, "`n")
	global maxLoops := fileContents.Length
}

; Global constants
global centerX := 960 ; Center of the screen
global centerY := 540
global stepInterval := 250 ; Amount of time between each step
global stepPad := 50 ; Range to randomize the above value (see Pad() for more detail)
global keyHoldInterval := 75 ; How long to hold the key down (Since ffxiv doesn't recognize instantaneous presses)
global keyHoldPad := 25
global hasTwoActors := false
global clearOffHand := true
global mouseMoveSpeed := 10

; Temp globals
global inputX := 128 ; Set to Main Hand location by default (so we don't have to set up those in the Switch)
global inputY := 469 ; Set to Main Hand by default
global offHandInputX := inputX + 249 ; Set to Off Hand location, to clear out unneeded offhands
global offHandInputY := inputY + 0
global windowX:= inputX + 172
global windowY:= inputY - 57

Switch skiplistName
{
	Case "Earring":
		inputX := inputX + 258
		inputY := inputY + 50
		hasTwoActors := true
	Case "Necklace":
		inputX := inputX + 258
		inputY := inputY + 98
	Case "Bracelet":
		inputX := inputX + 258
		inputY := inputY + 146
	Case "Ring":
		inputX := inputX + 258
		inputY := inputY + 194
	Case "BRD", "SAM", "RDM", "AST", "MNK", "DNC", "MCH", "NIN":
		clearOffHand := false
	Case "Shield":
		inputX := offHandInputX
		inputY := offHandInputY
}

; Progress bar stuff
; Save start time, to perform calculations with later
global macroStartTime := 0
global loopStartTime := 0

; Create progress bar to give option to abort macro
global counter := 0
global MyGui := Gui("AlwaysOnTop")
MyGui.Add("text", "w90 xm", "Current Loop Time: ")
loopTimeLabel := MyGui.Add("text", "w170 x+m", "Waiting to begin...")
MyGui.Add("text", "w103 xm", "Overall Elapsed Time: ")
elapsedTimeLabel := MyGui.Add("text", "w160 x+m", "Waiting to begin...")
MyGui.Add("text", "w128 xm", "Estimated Time Remaining: ")
timeRemainingLabel := MyGui.Add("text", "w137 x+m", "Waiting to begin...")
MyGui.Add("text", "w40 xm", "Progress:")
progressBarLabel := MyGui.Add("text", "w240 x+m", counter " / " maxLoops "  (" maxLoops " remain)")
progressBarProgress := MyGui.Add("Progress", "w300 xm Range0-%maxLoops% BackgroundGray")
ButtonStart :=MyGui.Add("Button",, "Start")
ButtonStart.OnEvent("Click", StartButton)
ButtonCancel := MyGui.Add("Button", "x+m Default", "Cancel")
ButtonCancel.OnEvent("Click", CancelButton)
MyGui.Show
return

StartButton(*)
{
	counter := 0
	global macroStartTime := A_TickCount
	ControlLoop()
}

CancelButton(*)
{
	ExitApp
}

ControlLoop()
{
	; The full UI loop is:
	; Click, arrow down, close. Move mouse, scroll down, click 
	; arrow *up*, arrow down, close, hide ui, screenshot, show ui

	; FFXIV had weird activation issues without this.
	Sleep 100
	WinActivate "ahk_exe ffxiv_dx11.exe"
	MouseMove inputX, inputY, mouseMoveSpeed
	Sleep 100

	For modelLine in fileContents {
		global counter
		global loopStartTime := A_TickCount
		loopStartTime := A_TickCount
		splitLine := StrSplit(modelLine, ", ")

		Switch skiplistName
		{
			Case "Earring", "Necklace", "Bracelet", "Ring":
				EnterModel(inputX, inputY, splitLine[1], splitLine[2])
			Default:
				if splitLine.Length > 4
				{
					EnterModel(inputX, inputY, splitLine[1], splitLine[2], splitLine[3])
					SkipToOffHand()
					EnterModel(offHandInputX, offHandInputY, splitLine[5], splitLine[6], splitLine[7], false)
				}
				else
				{
					EnterModel(inputX, inputY, splitLine[1], splitLine[2], splitLine[3])
					SkipToOffHand()
					EnterModel(offHandInputX, offHandInputY, 0, 0, 0, false)
				}
		}
		; Select next item
		; Click first input box
		; Enter splitLine[0]
		; tab
		; enter splitLine[1]
		; if not jewelry
		; tab
		; enter splitLine[2]
		; check if we need to also enter offhand, or if it happens automatically

		if hasTwoActors {

			; Move mouse
			Sleep Pad(stepInterval, stepPad)
			MouseMove centerX, centerY, mouseMoveSpeed
		
			; Scroll down
			Sleep Pad(stepInterval, stepPad)
			Send "{WheelDown 1}"
		
			; Select next item
			Switch skiplistName
			{
				Case "Earring", "Necklace", "Bracelet", "Ring":
					EnterModel(inputX, inputY, splitLine[1], splitLine[2])
				Case "MNK", "BRD", "MCH", "AST", "NIN", "RDM", "SAM", "DNC":
					EnterModel(inputX, inputY, splitLine[1], splitLine[2], splitLine[3])
					EnterModel(offHandInputX, offHandInputY, splitLine[5], splitLine[6], splitLine[7])
				Default:
					EnterModel(inputX, inputY, splitLine[1], splitLine[2], splitLine[3])
			}
		
			; Move mouse
			Sleep Pad(stepInterval, stepPad)
			MouseMove centerX, centerY, mouseMoveSpeed
		
			; Scroll down
			Sleep Pad(stepInterval, stepPad)
			Send "{WheelDown 1}"

		}

		; Hide UI and take screenshot (If dry run, just pause for a second, to visually verify results)
		if not (isDryRun) {

			LeftClick(windowX, windowY)
			HideUI()
			TakeScreenshot()
			ShowUI()
			
		} else {
			Sleep Pad(1000,0)
		}

		; Increment counter
		counter := counter + 1
		; Update GUI
		remaining := maxLoops - counter
		progressBarLabel.Value := counter " / " maxLoops "  (" remaining " remain)"
		progressBarProgress.Value := (counter/maxLoops)*100
		UpdateCurrentLoopTimer()
		UpdateTimeRemainingTimer()
	}
}


EnterModel(fieldX, fieldY, modelValue1, modelValue2, modelValue3 := -1, clickField := true)
{
	if (clickField)
	{
		LeftClick(fieldX, fieldY)
		Sleep Pad(stepInterval, stepPad)
	}
	; Send "{Backspace}"
	; Sleep Pad(stepInterval, stepPad)
	SendText modelValue1
	Sleep Pad(stepInterval, stepPad)
	Send "{Tab}"
	Sleep Pad(stepInterval, stepPad)
	SendText modelValue2
	Sleep Pad(stepInterval, stepPad)
	Send "{Tab}"
	Sleep Pad(stepInterval, stepPad)
	if not modelValue3 = -1
	{
		SendText modelValue3
		Sleep Pad(stepInterval, stepPad)
		Send "{Tab}"
		Sleep Pad(stepInterval, stepPad)
	}
}

SkipToOffHand()
{
	loop 10
	{
		Send "{Tab}"
		Sleep Pad(stepInterval, stepPad)
	}
}

LeftClick(xCoord := inputX, yCoord := inputY)
{
	; Click
	Sleep Pad(stepInterval, stepPad)
	MouseMove xCoord, yCoord, mouseMoveSpeed
	Sleep Pad(stepInterval, stepPad)
	Click "Down"
	Sleep Pad(keyHoldInterval, keyHoldPad)
	Click "Up"
}

HideUI()
{
	; Hide UI
	Sleep Pad(stepInterval, stepPad)
	Send "{shift down}"
	; Sleep Pad(keyHoldInterval, keyHoldPad)
	Send "{x down}"
	Sleep Pad(keyHoldInterval, keyHoldPad)
	Send "{x up}"
	; Sleep Pad(keyHoldInterval, keyHoldPad)
	Send "{shift up}"	
}

TakeScreenshot()
{
	; Take screenshot
	Sleep Pad(stepInterval, stepPad)
	Send "{shift down}"
	; Sleep Pad(keyHoldInterval, keyHoldPad)
	Send "{NumpadAdd down}"
	Sleep Pad(keyHoldInterval, keyHoldPad)
	Send "{NumpadAdd up}"
	; Sleep Pad(keyHoldInterval, keyHoldPad)
	Send "{shift up}"
	;Send "{PrintScreen}"
}

ShowUI()
{
	; Show UI
	Sleep Pad(stepInterval, stepPad)
	Send "{shift down}"
	; Sleep Pad(keyHoldInterval, keyHoldPad)
	Send "{x down}"
	Sleep Pad(keyHoldInterval, keyHoldPad)
	Send "{x up}"
	; Sleep Pad(keyHoldInterval, keyHoldPad)
	Send "{shift up}"
	; Wait to repeat
	Sleep Pad(stepInterval, stepPad)
}

Pad(initialNumber, padRange)
{
	; Generates a random value between the negative pad value and the positive pad value and adds it to the initial number
	; This should help humanize the macro ever so slightly.
	padAmount := Random(0-padRange, padRange)
	if (padAmount = -50)
		padAmount := -25

	paddedNumber := initialNumber + padAmount
	return paddedNumber
}

UpdateCurrentLoopTimer()
{
	; Calculate time elapsed in current loop
	loopTime := Round((A_TickCount - loopStartTime)/1000)
	loopTimeLabel.Value := FormatSeconds(loopTime, false)
	; Calculate time elapsed since beginning
	overallTicks := A_TickCount - macroStartTime
	overallTime := round(overallTicks/1000)
	elapsedTimeLabel.Value := FormatSeconds(overallTime)
}

UpdateTimeRemainingTimer()
{
	; Estimate time remaining
	overallTicks := A_TickCount - macroStartTime
	averageTime := overallTicks / counter
	secondsRemaining := round((averageTime * (maxLoops-counter))/1000)
	if (secondsRemaining = 0)
	{
		timeRemainingLabel.Value := "N/A"
	} else 
	{
		timeRemainingLabel.Value := FormatSeconds(secondsRemaining) . " (" . FormatSeconds(averageTime/1000, false) . " / loop)"
	}
}

FormatSeconds(NumberOfSeconds, HasHours:=true)
{
	time := 19990101  ; *Midnight* of an arbitrary date.
    time := DateAdd(time, NumberOfSeconds, "Seconds")
    mmss := FormatTime(time, "mm:ss")
    if(HasHours)
    {
    	return NumberOfSeconds//3600 ":" mmss
    }	
    else
    {
    	return mmss
    }
}

+Esc::ExitApp