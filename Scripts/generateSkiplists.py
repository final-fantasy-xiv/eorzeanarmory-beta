# Loop through list, increment until we hit next unique for that job, then start new counter

# Skiplist spec:

# A skiplist helps with automating the taking of screenshots, since the
# weapons dropdown in Ktisis can't be filtered. The list will contain an
# array of numbers, used to determine how many times to repeat the down
# arrow key press between each item for a given class/job. Each file will
# also end with an "X", for the script to detect when there are no more
# loops to be done.

import ast
import csv
import os.path
from pathlib import Path
from collections import defaultdict

# Mosty for reference
filteredColumns = ["#", "Name", "AllClassJobs", "ClassJobString", "ScreenshotClassJob", "Level{Equip}", "Level{Item}",
                   "EquipSlotCategory", "Model{Main}", "Model{Sub}", "ModelString", "IsFirstOfModel", "SharedModels",
                   "ColorString", "IsFirstOfColor", "AlternateVersions", "WebName", "LodestoneID", "IsCrestWorthy",
                   "IsDyeable", "IsGlamourous", "IsUnique", "IsUntradable", "Gallery", "IsInMultipleGalleries"]
filenames = defaultdict(list)
jobModels = defaultdict(list)

def main():
	class13Jobs = set()
	# Set our paths
	currentDirectory = Path.cwd()
	outputDirectory = currentDirectory/'Output'/'Skiplists'
	datafile = (currentDirectory/'FormattedItems.tsv').resolve()

	with open(datafile) as openDataFile:
		itemCounts = {}
		reader = csv.reader(openDataFile, delimiter="\t")
		for row in reader:
			# Turn item row into a dict, so we can refer to things easier
			itemDict = processRow(row)
			equipSlotCategory = int(itemDict["EquipSlotCategory"])

			match equipSlotCategory:
				case 1:
					# Single hand weapons (ie. PLD Swords)
					for classJob in itemDict["AllClassJobs"]:
						modelString = itemDict["Model{Main}"]
						if not itemDict["Model{Sub}"] == "0, 0, 0, 0":
							modelString = modelString + ', ' + itemDict["Model{Sub}"]
							class13Jobs.add(classJob)
						AddToLists(classJob, itemDict["IsFirstOfModel"], itemDict["IsFirstOfColor"], modelString, itemDict["WebName"])
				case 2:
					# Shields
					usesShields = ["PLD", "WHM", "BLM"]
					added = False
					for job in usesShields:
						if job in itemDict["AllClassJobs"] and added == False:
							AddToLists("Shield", itemDict["IsFirstOfModel"], itemDict["IsFirstOfColor"], itemDict["Model{Main}"], itemDict["WebName"])
							added = True
				case 13:
					# Two hand weapons (ie. BLM Staves)
					# Add Model{Sub} to ItemFiltered, and add it to this model string (eg '2601, 35, 1, 0, 2651, 35, 1, 0')
					for classJob in itemDict["AllClassJobs"]:
						modelString = itemDict["Model{Main}"]
						if not itemDict["Model{Sub}"] == "0, 0, 0, 0":
							modelString = modelString + ', ' + itemDict["Model{Sub}"]
							class13Jobs.add(classJob)
						AddToLists(classJob, itemDict["IsFirstOfModel"], itemDict["IsFirstOfColor"], modelString, itemDict["WebName"])
				case 9 | 10 | 11 | 12:
					# Accessories
					AccessoryType = getGearType(equipSlotCategory)
					AddToLists(AccessoryType, itemDict["IsFirstOfModel"], itemDict["IsFirstOfColor"], itemDict["Model{Main}"], itemDict["WebName"])
				# case _:
				# 	# Don't worry about other item types
				# 	continue

		# Save skiplists
		for job in jobModels:
			print("Saving "+job+".txt with "+str(len(jobModels[job]))+" lines.")
			outfileName = outputDirectory/(job + ".txt")
			if not outfileName.exists():
				if not os.path.exists(outputDirectory):
					os.makedirs(outputDirectory)
				outfile = open(outfileName, "x")
			else:
				outfile = open(outfileName, "w")
			outfile.write('\n'.join(str(line) for line in jobModels[job]))
			outfile.close()
		# Save list
		for job in filenames:
			print("Saving "+job+"-filenames.txt with "+str(len(filenames[job]))+" lines.")
			outfileName = outputDirectory/'Filenames'/(job + ".txt")
			if not outfileName.exists():
				if not os.path.exists(outputDirectory/'Filenames'):
					os.makedirs(outputDirectory/'Filenames')
				outfile = open(outfileName, "x")
			else:
				outfile = open(outfileName, "w")
			outfile.write('\n'.join(str(line) for line in filenames[job]))
			outfile.close()

		# Print list of jobs with separate main- and off-hand models
		# List them with quotes around them, so I can just copy and paste into the Autohotkey script
		jobList = ""
		for job in class13Jobs:
			if jobList == "":
				jobList = "\"" + job + "\""
			else:
				jobList = jobList + ", \"" + job + "\""
		print(f"The following jobs have separate Main- and Off-Hand models:\n{jobList}")

def AddToLists(ItemList, IsFirstOfModel, IsFirstOfColor, ModelString, WebName):
	if IsFirstOfModel or IsFirstOfColor:
		if ModelString not in jobModels[ItemList]:
			jobModels[ItemList].append(ModelString)
			filenames[ItemList].append(WebName + '.webp')
		else:
			print(f"Item {1} marked as first of Model or Color, but already present in Model list.", WebName)
	return

def getGearType(equipSlot, screenshotClassJob = 'PLD'):
	equipSlot = int(equipSlot)
	if equipSlot in [1, 2, 13]:
		if screenshotClassJob in ['CRP', 'BSM', 'ARM', 'GSM', 'LTW', 'WVR', 'ALC', 'CUL', 'MIN', 'BTN', 'FSH']:
			return 'Tool'
		else:
			return 'Weapon'
	elif equipSlot in [9, 10, 11, 12]:
		match equipSlot:
			case 9:
				return 'Earring'
			case 10:
				return 'Necklace'
			case 11:
				return 'Bracelet'
			case 12:
				return 'Ring'
		return 'Accessory'
	else:
		return 'Body'

def processRow(rowToProcess):
	itemDict = {}
	for (key, value) in zip(filteredColumns, rowToProcess):
		if key.startswith('Is'):
			# Handle booleans
			itemDict[key] = value == 'True'
		elif key == 'AllClassJobs' or key == 'SharedModels' or key == 'AlternateVersions':
			# Handle lists
			itemDict[key] = ast.literal_eval(value) # TODO: Look into implementing this differently.
		else:
			# Everything else is a string
			itemDict[key] = value

	return itemDict

main()