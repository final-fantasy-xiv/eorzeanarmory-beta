export default function(eleventyConfig) {
    // default layout
    eleventyConfig.addGlobalData("layout", "base_layout.njk");
    
    // TODO: some nice image handling and maybe thumbnail generation
    eleventyConfig.addPassthroughCopy("content/images");

    // TODO: better handling of basically all of this somehow probably
    eleventyConfig.addPassthroughCopy("content/.htaccess");
    eleventyConfig.addPassthroughCopy("content/apple-touch-icon.png");
    eleventyConfig.addPassthroughCopy("content/favicon.ico");
    eleventyConfig.addPassthroughCopy("content/favicon.svg");
    eleventyConfig.addPassthroughCopy("content/google-touch-icon.png");
    eleventyConfig.addPassthroughCopy("content/index.html");
    eleventyConfig.addPassthroughCopy("content/manifest.json");
    eleventyConfig.addPassthroughCopy("content/mask-icon.svg");
    eleventyConfig.addPassthroughCopy("content/navbar.css");
    eleventyConfig.addPassthroughCopy("content/style.css");
    eleventyConfig.addPassthroughCopy("content/w3.css");

    return {
        dir: {
            input: "content",
            output: "public"
        }
    }
}